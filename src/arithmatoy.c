#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { 
  return "0123456789abcdefghijklmnopqrstuvwxyz";
}

const size_t ALL_DIGIT_COUNT = 36;

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  // Retrieve the length of lhs and rhs and the maximum length
  unsigned int lhs_length = strlen(lhs);
  unsigned int rhs_length = strlen(rhs);
  unsigned int max_length = lhs_length > rhs_length ? lhs_length : rhs_length;

  // Allocate memory for the result
  char *result = (char *)malloc(max_length + 2);

  unsigned int carry = 0;

  // Loop through the digits of lhs and rhs
  for (unsigned int i = 0; i < max_length; i++) {
    unsigned int lhs_digit = 0;
    unsigned int rhs_digit = 0;

    if (lhs_length > i) {
      lhs_digit = get_digit_value(lhs[lhs_length - i - 1]);
    }

    if (rhs_length > i) {
      rhs_digit = get_digit_value(rhs[rhs_length - i - 1]);
    }

    // Add the digits and the carry
    unsigned int sum = lhs_digit + rhs_digit + carry;
    carry = sum / base;
    result[max_length - i] = to_digit(sum % base);
  }

  // Add the last carry if present
  if (carry > 0) {
    result[0] = to_digit(carry);
  } else {
    result = result + 1;
  }

  // Drop leading zeros if present and return the result
  return drop_leading_zeros(result);
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Drop leading zeros from lhs and rhs
  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);

  // Retrieve the length of lhs and rhs
  const size_t lhs_length = strlen(lhs);
  const size_t rhs_length = strlen(rhs);

  // Check if the result will be negative
  if(lhs_length < rhs_length)
    return NULL;

  // Allocate memory for the result
  char * result = calloc(lhs_length + 1, sizeof(char));

  unsigned int lhs_digit, rhs_digit;
  int diff;
  unsigned int carry = 0;

  // Loop through the digits of lhs and rhs
  for (size_t i = 0; i < lhs_length; i++){
    lhs_digit = get_digit_value(lhs[lhs_length - i - 1]);
    rhs_digit = i < rhs_length ? get_digit_value(rhs[rhs_length - i - 1]) : 0;

    diff = lhs_digit - rhs_digit - carry;

    // Check if there is a carry
    if(diff < 0){
      carry = 1;
      diff += base;
    }
    else{
      carry=0;
    }

    result[lhs_length - i -1] = to_digit(diff);
  }

  // Check if there is a carry left, if so, the result is negative
	if (carry != 0){
		free(result);
		return NULL;
	}

  // Drop leading zeros if present and return the result
	result = (char *) drop_leading_zeros(result);
	char * result_final = calloc(strlen(result) + 1, sizeof(char));
	strcpy(result_final, result);

	return result_final;
}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Retrieve the length of lhs and rhs
  size_t lhs_length = strlen(lhs);
  size_t rhs_length = strlen(rhs);

  // Allocate memory for the result
  size_t result_length = lhs_length + rhs_length;
  char *result = (char *)malloc((result_length + 1) * sizeof(char));

  // Initialize result with '0's
  for (size_t i = 0; i < result_length; i++) {
    result[i] = '0';
  }
  result[result_length] = '\0';

  // Loop through the digits of lhs
  for (size_t i = 0; i < lhs_length; ++i) {
    unsigned int lhs_value = get_digit_value(lhs[lhs_length - i - 1]);
    unsigned int carry = 0;

    // Loop through the digits of rhs
    for (size_t j = 0; j < rhs_length; ++j) {
      unsigned int rhs_value = get_digit_value(rhs[rhs_length - j - 1]);

      // Multiply the digits and add the carry
      unsigned int product = lhs_value * rhs_value + carry + get_digit_value(result[result_length - i - j - 1]);
      carry = product / base;
      unsigned int digit = product % base;

      result[result_length - i - j - 1] = to_digit(digit);
    }

    // Add the carry to the remaining digits
    if (carry > 0) {
      result[result_length - i - rhs_length - 1] = to_digit(carry);
    }
  }

  // Drop leading zero if present
  const char *trimmed_result = drop_leading_zeros(result);
  size_t trimmed_length = strlen(trimmed_result);

  // Copy the trimmed result into a new string
  char *result_final = (char *)malloc((trimmed_length + 1) * sizeof(char));
  strcpy(result_final, trimmed_result);

  return result_final;


}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
